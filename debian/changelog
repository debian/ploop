ploop (1.15-13) unstable; urgency=medium

  * QA upload.
  * Move u?mount.ploop out of /sbin
  * Remove baseless lintian-overrides
  * Replace obsolete build dependency with libext2fs-dev

 -- Chris Hofstaedtler <zeha@debian.org>  Sun, 26 Nov 2023 14:06:43 +0100

ploop (1.15-12) unstable; urgency=medium

  * QA upload.
  * Fix ubuntuized maintainer -- that list bounces.
  * Rebuild with debhelper 13.9.1.
  * Update lintian overrides to the new format.

 -- Adam Borowski <kilobyte@angband.pl>  Fri, 07 Oct 2022 16:35:17 +0200

ploop (1.15-11) unstable; urgency=medium

  * Ignore format-truncation errors to fix build failure. Closes: #977279.

 -- Steve Langasek <vorlon@debian.org>  Mon, 22 Aug 2022 20:14:08 +0000

ploop (1.15-10) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Update renamed lintian tag names in lintian overrides.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 28 May 2022 01:18:42 +0100

ploop (1.15-9) unstable; urgency=medium

  * QA upload.
  * Fix ftbfs with GCC-10. (Closes: #957691)
  * Update to compat level 13.
    - Install ploop.pc.
    - Remove execute permision from pkgconfig.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Thu, 02 Jul 2020 13:59:10 +0100

ploop (1.15-8) unstable; urgency=medium

  * QA upload.
  * Update Standards-Version to 4.5.0
  * Add Vcs link to salsa.
  * Fix FTCBFS. (Closes: #905741)
    - Thanks to Helmut Grohne.
  * Add symbols file for libploop1

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Sun, 23 Feb 2020 17:27:26 +0000

ploop (1.15-7) unstable; urgency=medium

  * QA upload.
  * Fix ftbfs with GCC-9. (Closes: #925803)
  * Update Standards-Version to 4.4.1
  * Update compat level to 12

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Sun, 29 Dec 2019 00:59:53 +0000

ploop (1.15-6) unstable; urgency=medium

  * QA upload.
  * Fix compilation with gcc 8. (Closes: #897835)
  * Drop unnecessary usage of dh-exec.
  * Enable all hardening options.
  * Simplify debian/watch and debian/rules.
  * Fix spelling errors found by lintian.
  * Use https in control and copyright.
  * Drop duplicated section field.
  * Bump debhelper compat/dependency to 11.
  * Bump Standards-Version to 4.1.5.
  * Use more appropriate homepage field.
  * Refresh patches and add headers.
  * Add upstream metadata file.
  * Drop lintian overrides for binaries that no longer exist.
  * Change ploop section to admin.
  * Add longer package descriptions.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 29 Jul 2018 00:01:58 +0200

ploop (1.15-5) unstable; urgency=medium

  * QA upload.
  * Add e2fsprogs to dependencies of libploop1 (Closes: #887298)

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 15 Jan 2018 16:57:26 +0100

ploop (1.15-4) unstable; urgency=medium

  * QA upload.
  * Fix FTBFS with glibc 2.25. (Closes: #882295)
  * Unhide the compiler flags during the build.
  * Priority extra -> optional.

 -- Adrian Bunk <bunk@debian.org>  Fri, 24 Nov 2017 21:07:16 +0200

ploop (1.15-3) unstable; urgency=low

  * Fix FTBFS issue, closes: #853619.
  * Maintainer changed to Debian QA Team.

 -- Ola Lundqvist <opal@debian.org>  Mon, 04 Sep 2017 21:42:34 +0200

ploop (1.15-2) unstable; urgency=high

  * Removed depdendency on hardening wrapper. Closes: #836646.
  * Removed vcs listing from control file as that is for upstream vcs.

 -- Ola Lundqvist <opal@debian.org>  Sun, 11 Sep 2016 22:01:27 +0200

ploop (1.15-1) unstable; urgency=low

  * New upstream release.
  * Updated debian/watch file from Bart Martens <bartm@debian.org>.

 -- Ola Lundqvist <opal@debian.org>  Sun, 19 Jun 2016 22:58:56 +0200

ploop (1.14.1-1) unstable; urgency=low

  * New upstream release.
  * Updated standards version to 3.9.6.
  * Added removal of lib/ploop.pc as it was not properly cleaned
    at make clean or make distclean targets.

 -- Ola Lundqvist <opal@debian.org>  Mon, 16 Nov 2015 19:49:14 +0100

ploop (1.13.2-1) unstable; urgency=low

  * New upstream release.

 -- Ola Lundqvist <opal@debian.org>  Tue, 21 Apr 2015 21:49:15 +0200

ploop (1.12.2-1) unstable; urgency=low

  * New upstream release.

 -- Ola Lundqvist <opal@debian.org>  Wed, 07 Jan 2015 20:33:13 +0100

ploop (1.12.1-1) unstable; urgency=medium

  * New upstream release.

 -- Ola Lundqvist <opal@debian.org>  Sun, 14 Sep 2014 13:54:37 +0200

ploop (1.12-1) unstable; urgency=low

  * New upstream release.

 -- Ola Lundqvist <opal@debian.org>  Wed, 20 Aug 2014 21:57:58 +0200

ploop (1.11-1) unstable; urgency=low

  * New upstream release.
  * Support for ia64 removed.
  * Removed dependency on util-linux, e2fsprogs and findutils as those packages
    are essential and will always be installed.

 -- Ola Lundqvist <opal@debian.org>  Fri, 18 Apr 2014 13:45:09 +0200

ploop (1.10-2) unstable; urgency=low

  * Fix ia64 build.

 -- Ola Lundqvist <opal@debian.org>  Fri, 27 Dec 2013 00:45:05 +0100

ploop (1.10-1) unstable; urgency=low

  * New upstream release. Now upstream includes the solution for #728173.

 -- Ola Lundqvist <opal@debian.org>  Mon, 23 Dec 2013 14:05:46 +0100

ploop (1.9-10) unstable; urgency=medium

  * Added parted, e2fsprogs and util-linux to the dependencies for
    libploop1.
  * Added recommendation for lsof and findutils (they are only used
    in special error cases).

 -- Ola Lundqvist <opal@debian.org>  Mon, 04 Nov 2013 21:19:10 +0100

ploop (1.9-9) unstable; urgency=low

  * Finally it is possible to build on ia64. Verified on a porter box.

 -- Ola Lundqvist <opal@debian.org>  Thu, 31 Oct 2013 19:48:35 +0100

ploop (1.9-8) unstable; urgency=low

  * One more fix for ia64. This time it is the '%lld' macro.

 -- Ola Lundqvist <opal@debian.org>  Thu, 31 Oct 2013 17:29:58 +0100

ploop (1.9-7) unstable; urgency=low

  * Build on ia64 is harder than it looks. More files need to be
    patched.

 -- Ola Lundqvist <opal@debian.org>  Wed, 30 Oct 2013 21:38:20 +0100

ploop (1.9-6) unstable; urgency=low

  * The previous fix for ia64 was not good enough. Better fix this time.

 -- Ola Lundqvist <opal@debian.org>  Wed, 30 Oct 2013 20:00:38 +0100

ploop (1.9-5) unstable; urgency=low

  * Fixed build problem on ia64 architecture.

 -- Ola Lundqvist <opal@debian.org>  Wed, 30 Oct 2013 19:18:19 +0100

ploop (1.9-4) unstable; urgency=low

  * Applied a patch from upstream that solves the problem with builds on
    architectures other than i386 and amd64, reported by Aaron M. Ucko
    <ucko@debian.org>. Closes: #728173.
  * Made the package arhitecture linux-any based on the information from
    upstream that there is nothing that should prevent it from being
    architecture agnostic.

 -- Ola Lundqvist <opal@debian.org>  Tue, 29 Oct 2013 21:12:00 +0100

ploop (1.9-3) unstable; urgency=low

  * Added dependency on pkg-config to make sure it builds in a minimal
    build environment. Closes: #728172.
  * Limited the architectures to i386 ia64 amd64 powerpc sparc which is
    the same as vzctl is limited to. Closes: #728174.

 -- Ola Lundqvist <opal@debian.org>  Tue, 29 Oct 2013 18:24:50 +0100

ploop (1.9-2) unstable; urgency=low

  * Applied patch from upstream to make sure /var/lock/ploop directory
    is created.

 -- Ola Lundqvist <opal@debian.org>  Sat, 05 Oct 2013 11:50:45 +0200

ploop (1.9-1) unstable; urgency=low

  * New upstream release.
  * Added hardening-wrapper to build depends.
  * Applied a patch from git to make this source build with hardening on.
  * Fixed possible rmdir error in postrm and prerm scripts.

 -- Ola Lundqvist <opal@debian.org>  Tue, 10 Sep 2013 14:21:02 +0200

ploop (1.8-1) unstable; urgency=low

  * Initial release (Closes: #720292)
  * Support for multiarch.

 -- Ola Lundqvist <opal@debian.org>  Tue, 20 Aug 2013 07:05:12 +0200
